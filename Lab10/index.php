<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8"/>
        <title>Lab 10</title>
        <link rel="stylesheet" href="style.css" />
    </head>
    
    <body>
        
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbName = "lab10";
        $tableName = "class";
          
        //connect server
        $conn = new mysqli($servername, $username, $password);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        
        //create database
        if (!$conn->query("CREATE database IF NOT EXISTS ". $dbName)){
            echo "Error creating database: " . $conn->error;
        }
        
        //connect the database
        if (!$conn->query("USE ". $dbName)){
            echo "Error using database: " . $conn->error;
        }
        
        //create table
        $tabCreate = "create table IF NOT EXISTS " . $tableName . "(
        id INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        firstname VARCHAR(30) NOT NULL,
        lastname VARCHAR(30) NOT NULL,
        matricular VARCHAR(15) 
        )";
        
        if (!$conn->query($tabCreate)){
            echo "Error creating table: " . $conn->error;
        }
        
        //print datas into a table
        function printTable($db, $table){
            $select = "SELECT * FROM " . $table;
            if ($result = $db->query($select)) {
                echo '<table id="classTable"><tr><th class="first">Name</th><th class="second">Password</th><th class="third">ID</th></tr><tr>';
                while ($row = $result->fetch_assoc()) {
                    echo "<tr class='row'><td class='first'>". $row["firstname"] . "</td><td class='second'>" . $row["lastname"] . "</td><td class='third'>" . $row["matricular"] . "</td></tr>";
                }
                echo '</table>';
                $result->free();
            }
        }
            
    ?>
        <header>
            <nav id="nav">
                <ul>
                    <li><a href='#' id="add">Sign in</a></li>
                    <li><a href='#' id="set">Change Password</a></li>
                </ul>
            </nav>
        </header>
        
        <main>
            <section id="forms">
                <div id="addForm">
                    <p><span class="error">* required field.</span></p>
                    <form method="post" action="add.php">
                        <table>
                            <tfoot><tr><td>
                                <input type="submit" name="addStudent" value="Add" id="addStudent"/>
                            </td></tr></tfoot>
                            <tbody>
                                <tr>
                                    <th><label for="fname">Name</label></th>
                                    <td><input type="text" name= "fname" id="fname"/>
                                        <span class="error">* <br><?php echo $nameErr;?></span></td>
                                </tr>
                                <tr>
                                    <th><label for="lname">Password</label></th>
                                    <td><input type="text" name= "lname" id="lname"/>
                                        <span class="error">* <br><?php echo $passErr;?></span></td>
                                </tr>
                                <tr>
                                    <th><label for="mat">ID</label></th>
                                    <td><input type="text" name= "mat" id="mat"/>
                                        <span class="error">* <br><?php echo $matErr;?></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                
                <div id="setForm">
                    <p><span class="error">* required field.</span></p>
                    <form method="post" action="set.php">
                        <table>
                            <tfoot><tr><td>
                                <input type="submit" name="setStudent" value="Set" id="setStudent"/>
                            </td></tr></tfoot>
                            <tbody>
                                <tr>
                                    <th><label for="mat">ID*</label></th>
                                    <td><input type="text" name= "mat" id="mat"/>
                                        <span class="error">* <br><?php echo $matErr;?></span></td>
                                </tr>
                                <tr>
                                    <th><label for="lname">New Password</label></th>
                                    <td><input type="text" name= "lname" id="lname"/>
                                        <span class="error">* <br><?php echo $passErr;?></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                
                
            </section>
            <br><br>
            <h1>Table Content</h1>
            <section id="tableSection">
                <?php
                    printTable($conn, $tableName);
                ?>
            </section>
        </main>
        
        <h1>Questions</h1>
        <div id="questions">
        <strong>1.</strong><br>
        It is good idea to separate model from control because it compels us to understand what we're doing and how we are doing things. It's a very good practice.<br><br>
        
        <strong>2.</strong><br>
        ODBC (Open Database Connectivity) is a standard application programming interface (API) for accessing database management systems (DBMS). The designers of ODBC aimed to make it independent of database systems and operating systems. An application written using ODBC can be ported to other platforms, both on the client and server side, with few changes to the data access code.<br><br>
        
        <strong>3.</strong><br>
        Primary defenses :<br>
        <ul>
            <li>Use of Prepared Statements (Parameterized Queries)</li>
            <li>Use of Stored Procedures</li>
            <li>Escaping all User Supplied Input</li>
            <li>White List Input Validation</li>
        </ul>
        Additional defenses:<br>
        <ul>
            <li>Enforce Least Privilege</li>
            <li>Perform White List Input Validation</li>
        </ul>
        </div>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
        <script src="code.js" type="text/javascript"></script>
        
        <footer>
            <div>
            https://openclassrooms.com/courses/systeme-d-inscription-avec-validation-par-l-administrateur
            https://en.wikipedia.org/wiki/Open_Database_Connectivity
            https://www.owasp.org/index.php/SQL_Injection_Prevention_Cheat_Sheet
            </div>
            <br><br><br>
        </footer>
        
    </body>


</html>