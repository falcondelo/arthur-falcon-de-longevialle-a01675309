<?php

    $tableName = "class";
    $dbName = "lab10";

    // define variables and set to empty values
    $nameErr = $passErr = $matErr = "";
    $name = $pass = $mat = "";

    $conn = new mysqli("localhost", "root", "root", $dbName);
    if ($conn->connect_error)
            die("Connection failed: " . $conn->connect_error);

    //verify
    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }    

    function insertTable($db, $table, $fname, $lname, $mat){
        $insert = "INSERT into " . $table . " (firstname, lastname, matricular) VALUES(?,?,?)";
        
        
        $stmt = $db->prepare($insert);
        if(!$stmt->bind_param("sss",$fname, $lname, $mat))
            die("Blindage error: " . $stmt->error);
        if (!$stmt->execute()) 
            die("Execution failed: (" . $stmt->errno . ") " . $stmt->error);
    }
    
    
        
    if (empty($_POST['fname'])) {
    $nameErr = "Name is required";
    echo 'Try again';
    } else {
    $name = test_input(htmlspecialchars($_POST['fname']));
    }

    if (empty($_POST['lname'])) {
    $passErr = "Password is required";
    echo 'Try again';
    } else {
    $pass = test_input(htmlspecialchars($_POST['lname']));
    }

    if (empty($_POST['mat'])) {
    $matErr = "Enter an id";
    echo 'Try again';
    } else {
      $website = test_input(htmlspecialchars($_POST['mat']));
    }
    
          
    if(isset($_POST['fname']) AND isset($_POST['lname']) AND isset($_POST['mat'])){
        //Crypting password
        //$_POST['lname'] = sha1($_POST['lname']);
        insertTable($conn, $tableName,htmlspecialchars($_POST['fname']),htmlspecialchars($_POST['lname']),
                                                                        htmlspecialchars($_POST['mat']));
        include('index.php');
    }
    
?>
