<?php
    $tableName = "class";
    $dbName = "lab10";

    $conn = new mysqli("localhost", "root", "root", $dbName);
    if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);
    
    function searchForMat($db, $table, $mat){
        $select = "SELECT id, matricular FROM " . $table;
        if ($result = $db->query($select)) {
            while ($row = $result->fetch_assoc()) {
                if($row['matricular'] == $mat)
                    return $row["id"];
            }
            $result->free();
            return 0;
        }
        else die('Selection error ' . $result->error);
    }

    function setFName($db, $table, $id, $fname){
        $query = "UPDATE " . $table . " SET firstname='" . $fname . "' WHERE id=" . $id;
        if(!$stmt = $db->prepare($query))
            die("error preparing : " . $stmt->error);
        if(!$stmt->execute())
            die("Execution failed: (" . $stmt->errno . ") " . $stmt->error);
    }
        
    function setLName($db, $table, $id, $lname){
        $query = "UPDATE " . $table . " SET lastname='" . $lname . "' WHERE id=" . $id;
        $stmt = $db->prepare($query);
        if(!$stmt->execute())
            die("Execution failed: (" . $stmt->errno . ") " . $stmt->error);
    }

    function setMat($db, $table, $id, $mat){
        $query = "UPDATE " . $table . " SET matricular='" . $mat . "' WHERE id=" . $id;
        $stmt = $db->prepare($query);
        if(!$stmt->execute())
            die("Execution failed: (" . $stmt->errno . ") " . $stmt->error);
    }
    
    if (empty($_POST['mat'])) {
    $nameErr = "ID is required";
    echo 'Try again';
    } else {
    $name = test_input(htmlspecialchars($_POST['mat']));
    }
    
    if (empty($_POST['lname'])) {
    $nameErr = "Password is required";
    echo 'Try again';
    } else {
    $name = test_input(htmlspecialchars($_POST['lname']));
    }

    
    
    if(isset($_POST['mat']) AND isset($_POST['fname']) AND isset($_POST['lname'])){
        $id = searchForMat($conn, $tableName,$_POST['mat']);
        setFName($conn, $tableName, $id,  htmlspecialchars($_POST['fname']));
        setLName($conn, $tableName, $id,  htmlspecialchars($_POST['lname']));   
        include('index.php');
    }
    else if(isset($_POST['mat']) AND isset($_POST['fname'])){
        $id = searchForMat($conn, $tableName,$_POST['mat']);
        setFName($conn, $tableName, $id,  htmlspecialchars($_POST['fname']));
        include('index.php');
    } 
    else if(isset($_POST['mat']) AND isset($_POST['lname'])){
        $id = searchForMat($conn, $tableName,$_POST['mat']);
        setLName($conn, $tableName, $id,  htmlspecialchars($_POST['lname']));
        include('index.php');
    } 
?>
